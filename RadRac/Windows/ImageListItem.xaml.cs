﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace RADARC.Windows
{
    /// <summary>
    /// Interaction logic for ImageListItem.xaml
    /// </summary>
    public partial class ImageListItem : UserControl
    {
        public bool SetDefault { get; set; }
        public string ImageKey { get; set; }
        public string ImageValue { get; set; }
        public Point PlusContorl { get; set; }
        public double Angle { get; set; }
        public Point PlusSize { get; set; }
        public double SpaceSlipe { get; set; }
        public int SlideNo { get; set; }
        public int TotalSlides { get; set; }
        public ImageListItem()
        {
           InitializeComponent();
            SetDefault = true;
        }
    }
}
