﻿using RADARC.Code;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace RADARC.Windows
{
    /// <summary>
    /// Interaction logic for ValuesFunctional.xaml
    /// </summary>
    public partial class ValuesFunctional : Window
    {
        public ValuesFunctional()
        {
            InitializeComponent();
        }
        Functional fl;
        internal void GetTheValuse(List<PointXYZ> Epoints,List<PointXYZ> Tpoints,Functional f)
        {
            fl=f;
            if (Epoints.Count > 0 && Tpoints.Count > 0) { 
            if (Epoints.Count == Tpoints.Count ||Epoints.Count<Tpoints.Count)
            {
                for (var i=0;i<Epoints.Count;i++)
                {
                    var LabelName = new TextBlock { Text = Epoints[i].Name };
                    var LabelZ = new TextBlock { Text = Epoints[i].Z.ToString() };
                    var LabelY = new TextBlock { Text = Epoints[i].Y.ToString() };
                    var LabelX = new TextBlock { Text = Epoints[i].X.ToString() };
                    PointPanel.Children.Add(LabelName);
                    EXPanel.Children.Add(LabelX);
                    EYPanel.Children.Add(LabelY);
                    EZPanel.Children.Add(LabelZ);
                    var LabelZt = new TextBlock { Text =Tpoints[i].Z.ToString() };
                    var LabelYt = new TextBlock { Text = Tpoints[i].Y.ToString() };
                    var LabelXt = new TextBlock { Text = Tpoints[i].X.ToString() };
                    TXPanel.Children.Add(LabelXt);
                    TYPanel.Children.Add(LabelYt);
                    TZPanel.Children.Add(LabelZt);
                    ModelAxis m = new ModelAxis { Xe = Epoints[i].X, Xt = Tpoints[i].X, Ye = Epoints[i].Y, Yt = Tpoints[i].Y, Ze = Epoints[i].Z, Zt = Tpoints[i].Z };
                    Alpha.Children.Add(new TextBlock { Text = m.Theta.ToString() });
                    Phi.Children.Add(new TextBlock { Text = m.Phi.ToString() });
                }
            }
            }
            else if(Epoints.Count<Tpoints.Count)
            {
                for (var i = 0; i < Epoints.Count; i++)
                {
                    var LabelName = new TextBlock { Text = Epoints[i].Name };
                    var LabelZ = new TextBlock { Text = Epoints[i].Z.ToString() };
                    var LabelY = new TextBlock { Text = Epoints[i].Y.ToString() };
                    var LabelX = new TextBlock { Text = Epoints[i].X.ToString() };
                    PointPanel.Children.Add(LabelName);
                    EXPanel.Children.Add(LabelX);
                    EYPanel.Children.Add(LabelY);
                    EZPanel.Children.Add(LabelZ);
                    var LabelZt = new TextBlock { Text = Tpoints[i].Z.ToString() };
                    var LabelYt = new TextBlock { Text = Tpoints[i].Y.ToString() };
                    var LabelXt = new TextBlock { Text = Tpoints[i].X.ToString() };
                    EXPanel.Children.Add(LabelXt);
                    EYPanel.Children.Add(LabelYt);
                    EZPanel.Children.Add(LabelZt);
                    ModelAxis m = new ModelAxis { Xe = Epoints[i].X, Xt = Tpoints[i].X, Ye = Epoints[i].Y, Yt = Tpoints[i].Y, Ze = Epoints[i].Z, Zt = Tpoints[i].Z };
                    Alpha.Children.Add(new TextBlock { Text = m.Theta.ToString() });
                    Phi.Children.Add(new TextBlock { Text = m.Phi.ToString() });
                }
            }
            


        }

       public ValuesFunctional(List<PointXYZ> Epoints, List<PointXYZ> Tpoints, Functional f)
        {
            InitializeComponent();
            fl = f;
           
            if(f.TargetBitmap!=null)
            this.traget.Source = f.TargetBitmap as ImageSource;
            if(f.EntryBitMap!=null)
            this.entry.Source = f.EntryBitMap as ImageSource;
            if (Epoints.Count > 0 && Tpoints.Count > 0)
            {
                if (Epoints.Count == Tpoints.Count || Epoints.Count < Tpoints.Count)
                {
                    for (var i = 0; i < Epoints.Count; i++)
                    {
                        var LabelName = new TextBlock { Text = Epoints[i].Name };
                        var LabelZ = new TextBlock { Text = Epoints[i].Z.ToString() };
                        var LabelY = new TextBlock { Text = Epoints[i].Y.ToString() };
                        var LabelX = new TextBlock { Text = Epoints[i].X.ToString() };
                        PointPanel.Children.Add(LabelName);
                        EXPanel.Children.Add(LabelX);
                        EYPanel.Children.Add(LabelY);
                        EZPanel.Children.Add(LabelZ);
                        var LabelZt = new TextBlock { Text = Tpoints[i].Z.ToString() };
                        var LabelYt = new TextBlock { Text = Tpoints[i].Y.ToString() };
                        var LabelXt = new TextBlock { Text = Tpoints[i].X.ToString() };
                        TXPanel.Children.Add(LabelXt);
                        TYPanel.Children.Add(LabelYt);
                        TZPanel.Children.Add(LabelZt);
                        ModelAxis m = new ModelAxis { Xe = Epoints[i].X, Xt = Tpoints[i].X, Ye = Epoints[i].Y, Yt = Tpoints[i].Y, Ze = Epoints[i].Z, Zt = Tpoints[i].Z };
                        Alpha.Children.Add(new TextBlock { Text = m.toDegree(m.Theta).ToString() });
                        Phi.Children.Add(new TextBlock { Text = m.toDegree(m.Phi).ToString() });
                    }
                }
            }
            else if (Epoints.Count < Tpoints.Count)
            {
                for (var i = 0; i < Epoints.Count; i++)
                {
                    var LabelName = new TextBlock { Text = Epoints[i].Name };
                    var LabelZ = new TextBlock { Text = Epoints[i].Z.ToString() };
                    var LabelY = new TextBlock { Text = Epoints[i].Y.ToString() };
                    var LabelX = new TextBlock { Text = Epoints[i].X.ToString() };
                    PointPanel.Children.Add(LabelName);
                    EXPanel.Children.Add(LabelX);
                    EYPanel.Children.Add(LabelY);
                    EZPanel.Children.Add(LabelZ);
                    var LabelZt = new TextBlock { Text = Tpoints[i].Z.ToString() };
                    var LabelYt = new TextBlock { Text = Tpoints[i].Y.ToString() };
                    var LabelXt = new TextBlock { Text = Tpoints[i].X.ToString() };
                    EXPanel.Children.Add(LabelXt);
                    EYPanel.Children.Add(LabelYt);
                    EZPanel.Children.Add(LabelZt);
                    ModelAxis m = new ModelAxis { Xe = Epoints[i].X, Xt = Tpoints[i].X, Ye = Epoints[i].Y, Yt = Tpoints[i].Y, Ze = Epoints[i].Z, Zt = Tpoints[i].Z };
                    Alpha.Children.Add(new TextBlock { Text = m.toDegree(m.Theta).ToString() });
                    Phi.Children.Add(new TextBlock { Text = m.toDegree(m.Phi).ToString() });
                }
            }



        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
           
            PrintDialog printDialog = new PrintDialog();
            if (printDialog.ShowDialog() == true)
            {

                printDialog.PrintVisual(grid, "doc");
            }

         }
    }
}
