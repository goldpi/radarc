﻿using RADARC.Dicom;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace RADARC.Windows
{
    /// <summary>
    /// Interaction logic for ToolBox.xaml
    /// </summary>
    public partial class FunctionalToolBox : UserControl
    {
        public FunctionalToolBox()
        {
            InitializeComponent();
        }

        public void PopUp(object sender,RoutedEventArgs args)
        {
            ValuePass.C = CommandPrompt.MarkPlus;
        }

        private void WindowSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {

        }

        private void CenterSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {

        }

        private void ZoomSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {

        }

        private void window_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void window_Selected(object sender, RoutedEventArgs e)
        {

        }
    }
}
