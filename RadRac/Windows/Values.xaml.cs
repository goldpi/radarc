﻿using RADARC.Code;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace RADARC.Windows
{
    /// <summary>
    /// Interaction logic for Values.xaml
    /// </summary>
    public partial class Values : Window
    {
        public Values()
        {
            InitializeComponent();
        }



        internal void GetTheValuse(List<PointXYZ> points)//, Canvas element)
        {
           // Image.Children.Add(element);
            foreach (var item in points)
            {
                var LabelName = new TextBlock { Text=item.Name };
                var LabelZ = new TextBlock { Text = item.Z.ToString() };
                var LabelY = new TextBlock { Text = item.Y.ToString() };
                var LabelX = new TextBlock { Text = item.X.ToString() };
                PointPanel.Children.Add(LabelName);
                XPanel.Children.Add(LabelX);
                YPanel.Children.Add(LabelY);
                ZPanel.Children.Add(LabelZ);
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            PrintDialog printDialog = new PrintDialog();
                if (printDialog.ShowDialog() == true)
            { printDialog.PrintVisual(grid, "Values"); }

        }
    }
}
