using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Dicom.Imaging;
using System.Windows.Controls.Primitives;
using System.Windows.Markup;

namespace RADARC.Windows
{
    /// <summary>
    /// Interaction logic for Biopsy.xaml
    /// </summary>
    public partial class Biopsy : UserControl
    {

        bool PlusVisible = true;
        bool PlusSelected = false;
        bool ZVisible = true;
        bool ZSelected = false;
        int Name = 1;
        List<ContentControl> ControlLIst = new List<ContentControl>();
        public Biopsy()
        {
            InitializeComponent();
        }
        private void Image_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            var p = e.GetPosition(this.Image);
            // x.Content = p.X.ToString();
            // y.Content = p.Y.ToString();

        }

        internal void ChangeImage(DicomImage image)
        {
            Image.Source = image.RenderImageSource();
        }

        private void ControlContent_SizeChanged(object sender, SizeChangedEventArgs e)
        {

        }

        internal void DrawCircle()
        {
            var grid = new Grid
            {
                Name = "CirlceGrid" + Name,

                Height = 10,
                MaxHeight = 10,
                MinHeight = 10,
                Width = 10,
                MinWidth = 10,
                MaxWidth = 10,
                Margin = new Thickness(0)

            };
            var text = new TextBlock
            {
                Name = "CircleText" + Name,
                Text = "T" + Name.ToString(),
                FontSize = 8,
                Foreground = Brushes.Red

            };
            var circle = new Ellipse { Name = "Cirlce" + Name, Fill = Brushes.LightBlue, Height = 10, MaxHeight = 10, MinHeight = 10, Width = 10, MinWidth = 10, MaxWidth = 10 };
            grid.Children.Add(circle);
            grid.Children.Add(text);

            ContentControl adorner = new ContentControl
            {
                Name="Point"+Name,
                Content = grid,
                Style = (Style)(this.FindResource("DesignerItemStyle") as Style),

            };
            Name++;
            adorner.Width = 0;
            adorner.Height = 0;
            Canvas.SetLeft(adorner, 0);
            Canvas.SetTop(adorner, 0);
            ControlLIst.Add(adorner);
            paintSurface.Children.Add(adorner);
        }

        internal void MakePlusVisible()
        {
            PlusVisible = !PlusVisible;
            ControlContent.Visibility = PlusVisible ? Visibility.Visible : Visibility.Hidden;
            if (PlusVisible == false)
            {
                PlusSelected = false;
                Selector.SetIsSelected((ContentControl)ControlContent, PlusSelected);
            }

        }
        internal void MakeZVisible()
        {
            ZVisible = !ZVisible;
            Zaixs.Visibility = ZVisible ? Visibility.Visible : Visibility.Hidden;
            if (ZVisible == false)
            {
                ZSelected = false;
                Selector.SetIsSelected((ContentControl)Zaixs, ZSelected);
            }

        }
        private void ControlContent_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {

        }

        private void ControlContent_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            PlusSelected = !PlusSelected;
            Selector.SetIsSelected((ContentControl)sender, PlusSelected);
        }

        public Point ContorlPosition()
        {
            return new Point(Canvas.GetLeft(ControlContent), Canvas.GetTop(ControlContent));
        }

        public Point ControlSize()
        {
            return new Point(ControlContent.Width, ControlContent.Height);
        }

        private void Zaixs_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            ZSelected = !ZSelected;
            Selector.SetIsSelected((ContentControl)Zaixs, ZSelected);
        }

        internal void TestSValues( )
        {
            

            #region GetCrossShapeBounds
            var Top = new Point
            {
                X = Canvas.GetLeft(ControlContent),
                Y = Canvas.GetTop(ControlContent)
            };
            var Bottom = new Point
            {
                X = ControlContent.Width,
                Y = ControlContent.Height
            };
            var ratioX = Bottom.X / 200;
            var ratioY = Bottom.Y / 251;
           
            
            #endregion
            #region GetZShape
            var ZTop = new Point
            {
            X = Canvas.GetLeft(Zaixs),
            Y = Canvas.GetTop(Zaixs)
            };
            var zTVector = ZTop - Top;
            var ZVector = new Point
            {
                X = (zTVector.X / ratioX),
                Y = (zTVector.Y / ratioY)-50
            };
            var ZBottom = new Point
            {
                X = Zaixs.Width,
                Y=Zaixs.Height
            };
            double BottomOfZ = Canvas.GetRight(Zaixs);
            #endregion
            #region List
            var vl = new List<Code.PointXYZ>();
            foreach (var item in ControlLIst)
            {
                var point = new Point();
                point.X =( Canvas.GetLeft(item) + 25);
                point.Y = (Canvas.GetTop(item) + 25);
                var vector=point-Top;
                var vetor = new Point { X = (vector.X / ratioX), Y = (vector.Y / ratioY) };
                vl.Add(new Code.PointXYZ { Name = item.Name, X = (int)(vetor.X-100<0?(vetor.X-100)*(-1):vetor.X-100), Y = (int)vetor.Y-50, Z = (int)ZVector.Y });
            }

            #endregion

            Values win = new Values();
            win.GetTheValuse(vl);
            win.Show();
          

           
        }


    }
}
