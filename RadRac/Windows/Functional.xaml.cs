﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Dicom.Imaging;
using System.Windows.Controls.Primitives;
using System.Windows.Markup;


namespace RADARC.Windows
{
    /// <summary>
    /// top is entry
    /// bottom is target
    /// Interaction logic for Functional.xaml
    /// </summary>
    public partial class Functional : UserControl
    {
        bool PlusVisible = true;
        bool PlusSelected = false;
        bool FIRST = true;
        bool ZSelectedtop = false;
        bool ZSelectedBottom = false;
       
        int ZtopFrameIndex = 0;
        int ZbottomFrameIndex = 0;
        public int current {
            get; set;
        }
        int totalFrame = 1;
      //  DicomImage Current,EntryImage,TargetImage;
        internal BitmapSource  EntryBitMap, TargetBitmap;
        List<ContentControl> Entry = new List<ContentControl>();
        List<ContentControl> Calculated = new List<ContentControl>();
        int Name = 1;
        List<ContentControl> ControlLIst = new List<ContentControl>();
        public Functional()
        {
            InitializeComponent();
            current=0;
        }
        internal void ChangeImage(DicomImage image ,int count,int slideNo)
        {
            if (!FIRST)
            {
                if (current == ZtopFrameIndex)
                {
                    EntryBitMap = ConvertToBitmapSource(paintSurface);
                }
                if (current == ZbottomFrameIndex)
                {
                    TargetBitmap = ConvertToBitmapSource(paintSurface);
                }
            }else
            FIRST = false;
            
            Image.Source = image.RenderImageSource();
            current = slideNo;
            totalFrame = count;
            ChangeText();
            DrawThePoints();
            Name = 0;
        }

        public BitmapSource ConvertToBitmapSource(UIElement element)
        {
            var target = new RenderTargetBitmap((int)(element.RenderSize.Width), (int)(element.RenderSize.Height), 96, 96, PixelFormats.Pbgra32);
            var brush = new VisualBrush(element);

            var visual = new DrawingVisual();
            var drawingContext = visual.RenderOpen();


            drawingContext.DrawRectangle(brush, null, new Rect(new Point(0, 0),
                new Point(element.RenderSize.Width, element.RenderSize.Height)));

            drawingContext.Close();

            target.Render(visual);

            return target;
        }
        void ChangeText()
        {
            
            FrameNoTop.Text = ZtopFrameIndex>0 ? "Entry Slide is" + ZtopFrameIndex:"Entry slide Not Locked";
            FrameNoBottom.Text = ZbottomFrameIndex > 0 ? "Target Slide is" + ZbottomFrameIndex : "Target slide Not Locked";
        }
        private void DrawThePoints()
        {
            var noOfchilds = paintSurface.Children.Count;

            if (noOfchilds > 4)
                paintSurface.Children.RemoveRange(4, noOfchilds - 1);
            #region GetCrossShapeBounds
            var Top = new Point
            {
                X = Canvas.GetLeft(ControlContent),
                Y = Canvas.GetTop(ControlContent)
            };
            var Bottom = new Point
            {
                X = ControlContent.Width,
                Y = ControlContent.Height
            };
            var ratioX = Bottom.X / 200;
            var ratioY = Bottom.Y / 251;
            #endregion
            if (current != ZtopFrameIndex && current != ZbottomFrameIndex)
            {
                if (Entry.Count > 0 && ControlLIst.Count > 0)
                {
                    if (Entry.Count == ControlLIst.Count || Entry.Count > ControlLIst.Count)
                    {

                        for (int i = 0; i < Entry.Count; i++)
                        {
                            DrawNewCircle(Entry[i], ControlLIst[i],i+1,ratioY);
                        }
                    }
                    else
                    {
                        for (int i = 0; i < ControlLIst.Count; i++)
                        {
                            DrawNewCircle(Entry[i], ControlLIst[i],i+1,ratioY);
                        }
                    }
                }
            }
            else
                if (current == ZtopFrameIndex)
            {
                foreach (var item in Entry)
                {
                    paintSurface.Children.Add(item);
                }
                Name = Entry.Count > 0 ? Entry.Count : 1;
            }
            else
            {
                foreach (var item in ControlLIst)
                {
                    paintSurface.Children.Add(item);
                }
                Name = ControlLIst.Count > 0 ? ControlLIst.Count : 1;
                //TargetBitmap=
            }
        }
        void DrawNewCircle(ContentControl entry, ContentControl Target,int no,double ratio)
        {
            Name = no;
            var str = "P";
            var y2 = Canvas.GetTop(entry);
            var y1 = Canvas.GetTop(Target);

            var x2 = Canvas.GetLeft(entry);
            var x1 = Canvas.GetLeft(Target);
            var z2 =(  ZtopFrameIndex)*ratio;
            var z1 =( ZbottomFrameIndex)*ratio;
            var dd =( current)*ratio;
            var xx = Cordinate((int)x1, (int)x2, (int)(z1), (int)(z2), (int)(dd));
            var yy = Cordinate((int)y1, (int)y2, (int)(z1), (int)(z2), (int)(dd));
            var ddx = x1 + xx;
            var ddy = y1 + yy;
            if (xx < 0) xx *= -1;
            if (yy < 0) yy *= -1;
            #region Grid
            var grid = new Grid
            {
                Name = "CirlceGrid" + Name,

                Height = 10,
                MaxHeight = 10,
                MinHeight = 10,
                Width = 10,
                MinWidth = 10,
                MaxWidth = 10,
                Margin = new Thickness(0)

            };
            #endregion
            #region TextBlock
            var text = new TextBlock
            {
                Name = "CircleText" + no,
                Text = str + Name.ToString(),
                FontSize = 8,
                Foreground = Brushes.Red

            };
            #endregion
            #region Circle
            var circle = new Ellipse
            {
                Name = "Cirlce" + no,
                Fill = Brushes.LightCyan,
                Height = 10,
                MaxHeight = 10,
                MinHeight = 10,
                Width = 10,
                MinWidth = 10,
                MaxWidth = 10
            };
            grid.Children.Add(circle);
            grid.Children.Add(text);
            #endregion
            #region Adoner
            ContentControl adorner = new ContentControl
            {
                Name = "Point" + no,
                Content = grid,
                Style = (Style)(this.FindResource("DesignerItemStyle") as Style),

            };
            #endregion
       
            adorner.Width = 0;
            adorner.Height = 0;
            Canvas.SetLeft(adorner, (x1+ddx));
            Canvas.SetTop(adorner, (y1+ddy));
            

                paintSurface.Children.Add(adorner);
           
        }
        private void ControlContent_SizeChanged(object sender, SizeChangedEventArgs e)
        {

        }

        internal void DrawCircle()
        {
            var str = current == ZtopFrameIndex ? "e" : "t";
            Name = (current == ZtopFrameIndex ? Entry.Count : ControlLIst.Count)+1 ;
           
            #region Grid
            var grid = new Grid
            {
                Name = "CirlceGrid" + Name,

                Height = 10,
                MaxHeight = 10,
                MinHeight = 10,
                Width = 10,
                MinWidth = 10,
                MaxWidth = 10,
                Margin = new Thickness(0)

            };
            #endregion
            #region TextBlock
            var text = new TextBlock
            {
                Name = "CircleText" + Name,
                Text = str + Name.ToString(),
                FontSize = 8,
                Foreground = Brushes.Red

            };
            #endregion
            #region Circle
            var circle = new Ellipse { Name = "Cirlce" + Name,
                Fill = current == ZtopFrameIndex ? Brushes.LightBlue:Brushes.LightGreen,
                Height = 10, MaxHeight = 10,
                MinHeight = 10, Width = 10, MinWidth = 10, MaxWidth = 10 };
            grid.Children.Add(circle);
            grid.Children.Add(text);
            #endregion
            #region Adoner
            ContentControl adorner = new ContentControl
            {
                Name = "Point" + Name,
                Content = grid,
                Style = (Style)(this.FindResource("DesignerItemStyle") as Style),

            };
            #endregion
            Name++;
            adorner.Width = 0;
            adorner.Height = 0;
            Canvas.SetLeft(adorner, 0);
            Canvas.SetTop(adorner, 0);
            #region AddToSurface
            if (current != ZbottomFrameIndex && current != ZtopFrameIndex)
            {
                MessageBox.Show("Sorry Cannot add point to Non Entry or Non Target slide");
            }
            else
            {
                if (current == ZtopFrameIndex)
                    Entry.Add(adorner);
                else
                    ControlLIst.Add(adorner);

                paintSurface.Children.Add(adorner);
            }
            #endregion

        }

        internal void MakePlusVisible()
        {
            PlusVisible = !PlusVisible;
            ControlContent.Visibility = PlusVisible ? Visibility.Visible : Visibility.Hidden;
            if (PlusVisible == false)
            {
                PlusSelected = false;
                Selector.SetIsSelected((ContentControl)ControlContent, PlusSelected);
            }

        }
        internal void MakeZVisible()
        {
            //ZVisible = !ZVisible;
            //Zaixstop.Visibility = ZVisible ? Visibility.Visible : Visibility.Hidden;
            //if (ZVisible == false)
            //{
            //    ZSelected = false;
            //    Selector.SetIsSelected((ContentControl)Zaixstop, ZSelected);
            //}

        }
        private void ControlContent_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {

        }

        private void ControlContent_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            PlusSelected = !PlusSelected;
            Selector.SetIsSelected((ContentControl)sender, PlusSelected);
        }

      
        private void ZaxisBottomClick(object sender, MouseButtonEventArgs e)
        {
            ZSelectedBottom = !ZSelectedBottom;
            Selector.SetIsSelected((ContentControl)ZaixsBotom, ZSelectedBottom);
        }
        private void ZaxistopClick(object sender, MouseButtonEventArgs e)
        {
            ZSelectedtop = !ZSelectedtop;
            Selector.SetIsSelected((ContentControl)Zaixstop, ZSelectedtop);
        }

        internal void TestSValues()
        {

            #region GetCrossShapeBounds
            var Top = new Point
            {
                X = Canvas.GetLeft(ControlContent),
                Y = Canvas.GetTop(ControlContent)
            };
            var Bottom = new Point
            {
                X = ControlContent.Width,
                Y = ControlContent.Height
            };
            var ratioX = Bottom.X / 200;
            var ratioY = Bottom.Y / 251;


            #endregion
            #region GetZShape
            var ZTop = new Point
            {
                X = Canvas.GetLeft(Zaixstop),
                Y = Canvas.GetTop(Zaixstop)
            };
            var zTVector = ZTop - Top;
            var ZVector = new Point
            {
                X = (zTVector.X / ratioX),
                Y = (zTVector.Y / ratioY) - 50
            };
            var ZBottom = new Point
            {
                X = Zaixstop.Width,
                Y = Zaixstop.Height
            };
            double BottomOfZ =( Canvas.GetTop(ZaixsBotom)/ratioY)-50;
            #endregion
            #region List
            var TargetLIst = new List<Code.PointXYZ>();
            foreach (var item in ControlLIst)
            {
                var point = new Point();
                point.X = (Canvas.GetLeft(item) + 25);
                point.Y = (Canvas.GetTop(item) + 25);
                var vector = point - Top;
                var vetor = new Point { X = (vector.X / ratioX), Y = (vector.Y / ratioY) };
                TargetLIst.Add(new Code.PointXYZ { Name = item.Name, X = (int)(vetor.X - 100) , Y = (int)vetor.Y - 50, Z = (int)BottomOfZ });
            }
            var EntryList= new List<Code.PointXYZ>();
            foreach (var item in Entry)
            {
                var point = new Point();
                point.X = (Canvas.GetLeft(item) + 25);
                point.Y = (Canvas.GetTop(item) + 25);
                var vector = point - Top;
                var vetor = new Point { X = (vector.X / ratioX), Y = (vector.Y / ratioY) };
                EntryList.Add(new Code.PointXYZ { Name = item.Name, X = (int)(vetor.X - 100), Y = (int)vetor.Y - 50, Z = (int)ZVector.Y });
            }
            #endregion

            ValuesFunctional win = new ValuesFunctional(EntryList, TargetLIst, this);
            //win.GetTheValuse();
            win.Show();

        }

       

        private void Zaixstop_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            MessageBoxResult dr = MessageBox.Show("Sure", "Some Title", MessageBoxButton.YesNo);
            if (dr == MessageBoxResult.Yes)
            {
                ZtopFrameIndex = current;
                ChangeText();
            } 
        }

        private void ZaixsBotom_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            MessageBoxResult dr = MessageBox.Show("Sure", "Some Title", MessageBoxButton.YesNo);
            if (dr == MessageBoxResult.Yes)
            {
                ZbottomFrameIndex = current;
                ChangeText();
            }
        }

        static int Cordinate(int x1, int x2, int y1, int y2, int yc)
        {
            var slope = (float)(y1 - y2);
            slope /= (x1 - x2);
            var ret = (yc - y1) / slope;
            ret -= x1;
            return (int)ret;
        }
    }
}
