﻿using Dicom.Imaging;
using Microsoft.Win32;
using RADARC.Code;
using RADARC.Dicom;
using RADARC.Windows;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Xceed.Wpf.AvalonDock.Layout;

namespace RADARC
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        bool file = true;
        bool bio = false;
        int count = 1;
        int imageno = 1;
      //  internal static ListBox l { get; set; }
        DicomDirHelper dir;
        DicomImage image;
        double windowWidth = 0;
        double windowCentre = 0;
        LoadingWindow lw;
        ImageListItem selectedItem = null;
        bool Default = false;
        Point DefaultValue = new Point(0, 0);
        Point DefaultSize = new Point(50, 50);
        List<CrossPosition> CrossPoints = new List<CrossPosition>();
        public Biopsy BiopsyWindow;
        public Functional FunctionalWindow;
        public MainWindow()
        {
            InitializeComponent();
            DockManager.Theme = new Xceed.Wpf.AvalonDock.Themes.MetroTheme();
            ToolBox.WindowSlider.ValueChanged += WindowSlider_ValueChanged;
            ToolBox.CenterSlider.ValueChanged += CenterSlider_ValueChanged;
            ToolBox.PlusButton.Click += PlusButton_Click;
            ToolBox.SetForAll.Click += SetForAll_Click;
            ToolBox.AddCricle.Click += AddCricle_Click;
            ToolBox.window.SelectionChanged += window_SelectionChanged;
            this.Closed += CloseIt;

            ToolBox.Visibility = Visibility.Hidden;

        }
       
        private void window_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (image != null)
            {
                var values = ToolBox.window.SelectedValue.ToString();
                var centers = WindowValue.Windowvalues(values);
                windowCentre = centers.Center;
                windowWidth = centers.Width;
                ToolBox.WindowSlider.Value = windowWidth;
                ToolBox.CenterSlider.Value = windowCentre;
                BiopsyWindowCheck();
            }
        }
        private void CloseIt(object sender,EventArgs e)
        {
            Application.Current.Shutdown();
        }
        private void AddCricle_Click(object sender, RoutedEventArgs e)
        {
            if (bio)
                BiopsyWindow.DrawCircle();
            else
                FunctionalWindow.DrawCircle();
           // throw new NotImplementedException();
        }

        private void SetForAll_Click(object sender, RoutedEventArgs e)
        {
            if (bio)
                BiopsyWindow.TestSValues();
            else
                FunctionalWindow.TestSValues();
        }

        private void PlusButton_Click(object sender, RoutedEventArgs e)
        {
            if (BiopsyWindow != null)
                BiopsyWindow.MakePlusVisible();
        }

        private void CenterSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (image != null)
            {
                windowCentre = e.NewValue;
                BiopsyWindowCheck();
            }
        }

        private void WindowSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (image != null)
            {
                windowWidth = e.NewValue;
                BiopsyWindowCheck();
            }
           
        }

       
      
        public static void UpdateValue()
        {

           
        }

        public void Exit(object s,RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }
        public void OpenBiopsy(object s, RoutedEventArgs e)
        {
            OpenFileDialog f = new OpenFileDialog();
            if (file)
            {

                if (f.ShowDialog()??false)
                {
                    file = false;
                    bio = true;
                    FileOpen(f.FileName);
                    ToolBox.Visibility = Visibility.Visible;            
                }
            }
            else
            {
                MessageBox.Show("A Window is Already Open,Please Close It first!");
            }


           
        }
        private async void FileOpen(string path)
        {
            lw = new LoadingWindow();
            lw.Show();
            lw.loadingText.Content = "Loading file.";
            var p = Task<DicomDirHelper>.Factory.StartNew(() => new DicomDirHelper(path));
            await p;
            lw.loadingText.Content = "File Loding complete.Retriving Image List";
            dir = p.Result;
            var ff = dir.FileInfoDic.First();
            image = new DicomImage(ff.Value);

            windowWidth = image.WindowWidth;
            windowCentre = image.WindowCenter;
            ToolBox.WindowSlider.Value=windowWidth;
            ToolBox.CenterSlider.Value=windowCentre;
            var dispatcher = TaskScheduler.FromCurrentSynchronizationContext();          
            lw.loadingText.Content = "Generating thumbnails";
            int pcount = 0, Scount = 1, SerCount = 0, Image = 1;
            foreach (var item in dir.Paitent)
            {
                TreeViewItem lp = new TreeViewItem { Name ="Patient"+ pcount++ , Header=item.PaitentName};                
                foreach (var item2 in item.Study)
                {
                    TreeViewItem ls = new TreeViewItem { Name = "Study" + Scount, Header = "Study " + Scount };
                    Scount++;
                    foreach (var item3 in item2.Series.OrderBy(i=>i.SeriesName))
                    {
                        TreeViewItem lss = new TreeViewItem { Name = "Series" +SerCount ,Header= "Series " + SerCount };
                        SerCount++;
                        Image = 1;
                        var imgcount = item3.Images.Count();
                        foreach (var item4 in item3.Images.OrderBy(i=>i.Index))
                        {
                            var imgr = dir.FileInfoDic.SingleOrDefault(ig => ig.Key == item4.ReferencedSOPInstanceUIDInFile);
                            ImageListItem boxItem = new ImageListItem { ImageKey = item4.ReferencedSOPInstanceUIDInFile,
                                ImageValue = imgr.Value,
                                SpaceSlipe =item4.SliceSpace ,
                                SlideNo =Image,
                                TotalSlides=imgcount
                            };
                            var i = new DicomImage(imgr.Value);
                            i.Scale = .1;
                            boxItem.Img.Source = i.RenderImageSource();
                            boxItem.lbl.Content = "Image" + Image++;
                            boxItem.MouseDoubleClick += BoxItem_MouseDoubleClick;
                            lss.Items.Add(boxItem);
                            var ipr = new CrossPosition
                            {
                                Key = boxItem.ImageKey,
                                Position = new System.Drawing.Point(0, 0),
                                Size = new System.Drawing.Point(20, 20)
                            };
                            CrossPoints.Add(ipr);
                        }
                        ls.Items.Add(lss);
                    }
                    lp.Items.Add(ls);
                }
                files.Items.Add(lp);
            }
            BiopsyWindowCheck();
        }

        private void BiopsyWindowCheck()
        {
           
            image.WindowCenter = windowCentre;
            image.WindowWidth = windowWidth;
            #region ifTrue
            if(bio)
            if (BiopsyWindow == null)
            {
               
                BiopsyWindow = new Biopsy();
                LayoutDocument sx = new LayoutDocument();
                sx.IsActive = true;
                sx.Title = "Biopsy Window";
                
                sx.Content = BiopsyWindow;
                sx.Closed += CloseEdWindow;
                BiopsyWindow.ChangeImage(image);
                DcoumentPane.Children.Add(sx);
                lw.Hide();
            }
            else
            {
                if (selectedItem != null)
                {
                    selectedItem.PlusContorl=BiopsyWindow.ContorlPosition();
                    selectedItem.PlusSize = BiopsyWindow.ControlSize();
                    var p = CrossPoints.FirstOrDefault(i => i.Key == selectedItem.ImageKey);
                    p.Position = new System.Drawing.Point((int)selectedItem.PlusContorl.X ,(int) selectedItem.PlusContorl.Y);
                    p.Size= new System.Drawing.Point((int)selectedItem.PlusSize.X, (int)selectedItem.PlusSize.Y);
                }
                BiopsyWindow.ChangeImage(image);
            }
            #endregion
            else
           if (FunctionalWindow == null)
            {

                FunctionalWindow = new Functional();
                LayoutDocument sx = new LayoutDocument();
                sx.IsActive = true;
                sx.Title = "Functional Window";

                sx.Content = FunctionalWindow;
                sx.Closed += CloseEdWindow;
                FunctionalWindow.ChangeImage(image,count,imageno);
                DcoumentPane.Children.Add(sx);
                lw.Hide();
            }
            else
            {
                if (selectedItem != null)
                {
                    //selectedItem.PlusContorl = FunctionalWindow.ContorlPosition();
                    //selectedItem.PlusSize = FunctionalWindow.ControlSize();
                    var p = CrossPoints.FirstOrDefault(i => i.Key == selectedItem.ImageKey);
                    p.Position = new System.Drawing.Point((int)selectedItem.PlusContorl.X, (int)selectedItem.PlusContorl.Y);
                    p.Size = new System.Drawing.Point((int)selectedItem.PlusSize.X, (int)selectedItem.PlusSize.Y);
                }
                FunctionalWindow.ChangeImage(image,count,imageno);
            }
        }

        private void BoxItem_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {

            var Value = ((ImageListItem)sender).ImageValue.ToString();
           
            image = new DicomImage(Value);
            BiopsyWindowCheck();
            if (selectedItem != null)
            {
               
            }
            selectedItem = ((ImageListItem)sender);
            if (BiopsyWindow != null)
            {
                if (Default)
                {
                    //For default value for every window.
                    //Canvas.SetLeft(BiopsyWindow.ControlContent, DefaultValue.X);
                    //Canvas.SetTop(BiopsyWindow.ControlContent, DefaultValue.Y);
                    //BiopsyWindow.ControlContent.Height = DefaultSize.Y;
                    //BiopsyWindow.ControlContent.Width = DefaultSize.X;
                }
                else
                {
                    //for manupliation
                    //Canvas.SetLeft(BiopsyWindow.ControlContent, selectedItem.PlusContorl.X);
                    //Canvas.SetTop(BiopsyWindow.ControlContent, selectedItem.PlusContorl.Y);
                    //BiopsyWindow.ControlContent.Height = selectedItem.PlusSize.Y;
                    //BiopsyWindow.ControlContent.Width = selectedItem.PlusSize.X;
                }
            }
            //toDo resize and positon cotrol.
            //BiopsyWindow.ControlContent.
        }

        private void CloseEdWindow(object sender, EventArgs e)
        {
           if(sender is LayoutContent)
            {
                var p = sender as LayoutContent;
                if(p.Title== "Biopsy Window")
                {
                    BiopsyWindow = null;
                  
                }
                if (p.Title == "Functional Window")
                {
                    FunctionalWindow = null;
                }
                GC.Collect();
            }
           
        }
        /// <summary>
        /// Functional window launcher.
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        public void OpenFunctional(object s, RoutedEventArgs e)
        {
            OpenFileDialog f = new OpenFileDialog();
            if (file)
            {

                if (f.ShowDialog() ?? false)
                {
                    file = false;
                    bio = false;
                    FileOpen(f.FileName);
                    ToolBox.Visibility = Visibility.Visible;
                }
            }
            else
            {
                MessageBox.Show("A Window is Already Open,Please Close It first!");
            }


        }
        public void Close(object s, RoutedEventArgs e)
        {
            if (BiopsyWindow != null){
                DcoumentPane.Children.Clear();
                BiopsyWindow = null;
            }
            
        }

        private void files_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {

           
            if(e.NewValue is ImageListItem)
            {
                var Value = ((ImageListItem)e.NewValue).ImageValue.ToString();
                var item = ((ImageListItem)e.NewValue);
                image = new DicomImage(Value);
                count = item.TotalSlides;
                imageno = item.SlideNo;
                BiopsyWindowCheck();
                if (selectedItem != null)
                {

                }
                selectedItem = item;
            }
            
            
        }
    }
}
