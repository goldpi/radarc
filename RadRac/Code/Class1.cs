﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;

namespace RADARC.Code
{
    public class WindowValue
    {
        public double Center { get; set; }
        public double Width { get; set; }

        public static WindowValue Windowvalues(string Text)
        {
            var ret = new WindowValue();
            switch (Text.ToLower())
            {
                case "chest":
                    ret.Center = 56;
                    ret.Width = 342;
                    break;
                case "lung":
                    ret.Center = -498;
                    ret.Width = 1465;
                    break;
                case "liver":
                    ret.Center = 93;
                    ret.Width = 109;
                    break;
                case "bone":
                    ret.Center = 570;
                    ret.Width = 3077;
                    break;
                case "brain/sinus":
                    ret.Center = 42;
                    ret.Width = 155;
                    break;
                case "lervical":
                    ret.Center = 11;
                    ret.Width = 195;
                    break;
                case "ct lumber spine":
                    ret.Center = 108;
                    ret.Width = 877;
                    break;
                case "extremity":
                    ret.Center = 56;
                    ret.Width = 342;
                    break;
                case "foot":
                    ret.Center = 455;
                    ret.Width = 958;
                    break;
                case "femur":
                    ret.Center = 747;
                    ret.Width = 727;
                    break;
                case "auto":
                    ret.Center = 343.5;
                    ret.Width = 1955;
                    break;
                case "full":
                    ret.Center = -1024;
                    ret.Width = 65536;
                    break;

            }
            return ret;
        }
    }
    class PlusShape : Shape
    {
        protected override Geometry DefiningGeometry
        {
            get
            {
                if (SourceElement == null || TargetElement == null)
                {
                    return new PathGeometry();
                }

                Visual anc = SourceElement.FindCommonVisualAncestor(TargetElement) as Visual;

                if (anc == null)
                    throw new InvalidOperationException();

                GeneralTransform sourceOffsetTransform = SourceElement.TransformToAncestor(anc);
                GeneralTransform targetOffsetTransform = TargetElement.TransformToAncestor(anc);

                Point sourceOffset = sourceOffsetTransform.Transform(new Point(0, 0));
                Point targetOffset = targetOffsetTransform.Transform(new Point(0, 0));

                // Point order is: SourceElement top right corner, TargetElement top left corner,
                //                 TargetElement bottom left corner, SourceElement bottom right corner
                Point point1 = new Point(SourceElement.ActualWidth + sourceOffset.X, sourceOffset.Y);
                Point point2 = new Point(targetOffset.X, targetOffset.Y);
                Point point3 = new Point(targetOffset.X, TargetElement.ActualHeight + targetOffset.Y);
                Point point4 = new Point(SourceElement.ActualWidth + sourceOffset.X, SourceElement.ActualHeight + sourceOffset.Y);

                List<PathSegment> segments = new List<PathSegment>(4);
                segments.Add(new LineSegment(point2, false));
                segments.Add(new LineSegment(point3, false));
                segments.Add(new LineSegment(point4, false));
                segments.Add(new LineSegment(point1, false));

                List<PathFigure> figures = new List<PathFigure>(1);
                PathFigure pf = new PathFigure(point1, segments, true);
                figures.Add(pf);

                Geometry g = new PathGeometry(figures, FillRule.EvenOdd, null);
                return g;

            }
        }
        public FrameworkElement SourceElement
        {
            get { return (FrameworkElement)GetValue(SourceElementProperty); }
            set { SetValue(SourceElementProperty, value); }
        }

        public FrameworkElement TargetElement
        {
            get { return (FrameworkElement)GetValue(TargetElementProperty); }
            set { SetValue(TargetElementProperty, value); }
        }

        public static readonly DependencyProperty SourceElementProperty =
            DependencyProperty.Register("SourceElement", typeof(FrameworkElement),
                typeof(PlusShape), new UIPropertyMetadata(new PropertyChangedCallback(WireUpSource)));

        public static readonly DependencyProperty TargetElementProperty =
            DependencyProperty.Register("TargetElement", typeof(FrameworkElement),
                typeof(PlusShape), new UIPropertyMetadata(new PropertyChangedCallback(WireUpTarget)));

    
    private static void WireUpSource(DependencyObject d, DependencyPropertyChangedEventArgs e)
    {
        Debug.WriteLine("PlusShape dependency property changed");

        PlusShape ej = d as PlusShape;
        FrameworkElement el = e.NewValue as FrameworkElement;

        if (ej != null && el != null)
        {
            el.SizeChanged += new SizeChangedEventHandler(ej.FrameworkElement_SizeChanged);
            el.LayoutUpdated += new EventHandler(ej.SourceFrameworkElement_LayoutUpdated);
        }
    }

    private static void WireUpTarget(DependencyObject d, DependencyPropertyChangedEventArgs e)
    {
        Debug.WriteLine("PlusShape dependency property changed");

        PlusShape ej = d as PlusShape;
        FrameworkElement el = e.NewValue as FrameworkElement;

        if (ej != null && el != null)
        {
            el.SizeChanged += new SizeChangedEventHandler(ej.FrameworkElement_SizeChanged);
            el.LayoutUpdated += new EventHandler(ej.TargetFrameworkElement_LayoutUpdated);
        }

    }
    private Point _lastSourceOffset;
    private Point _lastTargetOffset;

    private void SourceFrameworkElement_LayoutUpdated(object sender, EventArgs e)
    {
        Debug.WriteLine("PlusShape source element layout updated - " + DateTime.Now.Ticks);

        Visual anc = SourceElement.FindCommonVisualAncestor(TargetElement) as Visual;
        GeneralTransform sourceOffsetTransform = SourceElement.TransformToAncestor(anc);

        Point sourceOffset = sourceOffsetTransform.Transform(new Point(0, 0));

        if (!sourceOffset.Equals(_lastSourceOffset))
        {
            // Only redraw if position has changed
            _lastSourceOffset = sourceOffset;
            this.InvalidateVisual();
        }
    }

    private void TargetFrameworkElement_LayoutUpdated(object sender, EventArgs e)
    {
        Debug.WriteLine("PlusShape target layout layout updated - " + DateTime.Now.Ticks);

        Visual anc = SourceElement.FindCommonVisualAncestor(TargetElement) as Visual;
        GeneralTransform targetOffsetTransform = TargetElement.TransformToAncestor(anc);

        Point targetOffset = targetOffsetTransform.Transform(new Point(0, 0));
        if (!targetOffset.Equals(_lastTargetOffset))
        {
            // Only redraw if position has changed
            _lastTargetOffset = targetOffset;
            this.InvalidateVisual();
        }
    }

    private void FrameworkElement_SizeChanged(object sender, SizeChangedEventArgs e)
    {
        Debug.WriteLine("PlusShape source or target size changed - instance");
        this.InvalidateVisual();

    }
}

    public class ModelAxis
    {
        public double Xt { get; set; }
        public double Xe { get; set; }
        public double Yt { get; set; }
        public double Ye { get; set; }
        public double Zt { get; set; }
        public double Ze { get; set; }

        public double Xd { get { return Xe - Xt; } }
        public double Yd { get { return Ye - Yt; } }
        public double Zd { get { return Ze - Zt; } }

        public double Distance
        {
            get
            {
                return Math.Sqrt(
                    (Xd * Xd)
                    + (Yd * Yd)
                    + (Zd * Zd)
                    );
            }
        }
        public double Theta
        {
            get
            {
                return Math.Acos(Zd / Distance);
            }
        }
        public double Phi
        {
            get
            {
                return Math.Atan(Yd / Xd);
            }
        }
        public double toDegree(double rad)
        {
            // var pi = (double)(22 / 7);
            return rad * (double)(180 / Math.PI);

        }
    }
}
