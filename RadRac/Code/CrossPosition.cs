﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RADARC.Code
{
    public class CrossPosition
    {
        public Point Position { get; set; }
        public Point Size { get; set; }
        public string Key { get; set; }
    }
}
