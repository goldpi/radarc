﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RADARC.Code
{
    public class PointXYZ
    {
        public string Name { get; set; }
        public int Z { get; set; }
        public int Y { get; set; }
        public int X { get; set; }
    }
}
