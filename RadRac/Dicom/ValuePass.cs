﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace RADARC.Dicom
{
    public class ValuePass
    {
        static ValuePass()
        {
            C = CommandPrompt.NoCommand;
        }
        static public CommandPrompt C { get; set; }
               
        static public double Angle { get; set; }

        static public Point Points { get; set; }
        static public Point Size { get; set; }
      


        public static Point Center
        {
            get
            {
               return new Point(
                    (Points.X + (Size.X / 2)) * Math.Sin(Angle), (Points.Y + (Size.Y / 2)) * Math.Cos(Angle));


            
            }
        }
    }

    public class ImageClass {

      
        public string Name { get; set; }
        public string ImageKey { get; set; }
        public string ImageValue { get; set; }
    }

    public enum CommandPrompt
    {
        NoCommand,
        MarkPlus,
        MarkPoint,
        MarkPointTagret,
        MarkPointEntry,
        MarkAZpoint,
        MarkBZpoint
    }
}
