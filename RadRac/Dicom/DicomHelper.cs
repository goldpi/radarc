﻿using Dicom;
using Dicom.Imaging;
using Dicom.Media;
using System;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Security.AccessControl;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace RADARC.Dicom
{
    
    public class DicomDirHelper
    {
        public void CopyFolder(DirectoryInfo source, DirectoryInfo target)
        {
            foreach (DirectoryInfo dir in source.GetDirectories())
                CopyFolder(dir, target.CreateSubdirectory(dir.Name));
            foreach (FileInfo file in source.GetFiles())
                file.CopyTo(System.IO.Path.Combine(target.FullName, file.Name));
        }
        public DicomDirHelper(string Path)
        {
            this.Path = Path;
            var di = new DirectoryInfo(Path);
            var i = DriveInfo.GetDrives().First(j => j.RootDirectory.Root.Name == di.Root.Name);
            // var driv=i.FirstOrDefault(ig => ig.Name == di.);
            if (i.DriveType == DriveType.CDRom)
            {
                MessageBox.Show("CD Rom detected: Please Wait..");
                this.Path = GetTemporaryDirectory();
                try
                {
                    CopyFolder(new DirectoryInfo(i.RootDirectory.Root.Name), new DirectoryInfo(this.Path));
                    this.Path += "\\DICOMDIR";
                }
                catch
                {
                    MessageBox.Show("Currupted CD");
                }
            }
            Paitent = new List<PaitentRecord>();
            DICOMDIR = DicomDirectory.Open(this.Path);

            //DICOMDIR.Format.
            Paitent = this.PaitentRecords(DICOMDIR).ToList();

            Dir = DirectoryOfDicomDir();
            PrepareDictionary();

        }
        public List<PaitentRecord> Paitent { get; set; }
        public string Dir { get; set; }
        public string Path { get; set; }
        private DicomDirectory DICOMDIR;
        public Dictionary<string, string> FileInfoDic = new Dictionary<string, string>();
        public Dictionary<string, int> file_z = new Dictionary<string, int>();
        public IEnumerable<PaitentRecord> PaitentRecords()
        {
            return PaitentRecords(this.DICOMDIR);
        }
        public IEnumerable<PaitentRecord> PaitentRecords(DicomDirectory dicomDirectory)
        {

            foreach (var patientRecord in dicomDirectory.RootDirectoryRecordCollection)
            {
                var Pr = new PaitentRecord
                {
                    PaitentName = patientRecord.Get<string>(DicomTag.PatientName),
                    Study = new List<StudyRecord>(),
                    DateOfBirth = patientRecord.Get<string>(DicomTag.PatientAge) ?? "",
                    PaitentId = patientRecord.Get<string>(DicomTag.PatientID) ?? "",
                    Sex = patientRecord.Get<string>(DicomTag.PatientSex) ?? "",
                    Diagnosis = patientRecord.Get<string>(DicomTag.AdmittingDiagnosesDescription) ?? "",
                    Procedure = patientRecord.Get<string>(DicomTag.ProcedureCodeSequence) ?? "",
                    Remarks = patientRecord.Get<string>(DicomTag.ProcedureStepProgressDescription) ?? ""
                };

                Pr.Study = StudyRecord(patientRecord).ToList();
                yield return Pr;
            }
        }

        private IEnumerable<StudyRecord> StudyRecord(DicomDirectoryRecord patientRecord)
        {
            foreach (var studyRecord in patientRecord.LowerLevelDirectoryRecordCollection)
            {
                var sr = new StudyRecord
                {
                    StudyDate = studyRecord.Get<string>(DicomTag.StudyDate) ?? "",
                    ReferringPhysicianName = studyRecord.Get<string>(DicomTag.ReferringPhysicianName) ?? "",
                    StudyID = studyRecord.Get<string>(DicomTag.StudyID) ?? "",
                    AccessionNumber = studyRecord.Get<string>(DicomTag.AccessionNumber) ?? "",
                    StudyTime = studyRecord.Get<string>(DicomTag.StudyTime) ?? ""
                };

                sr.StudyName = studyRecord.Get<string>(DicomTag.StudyInstanceUID);
                sr.Series = SeriesRecord(studyRecord).ToList();
                yield return sr;
            }
        }

        private IEnumerable<SeriesRecord> SeriesRecord(DicomDirectoryRecord studyRecord)
        {
            foreach (var seriesRecord in studyRecord.LowerLevelDirectoryRecordCollection)
            {
                var sr = new SeriesRecord
                {
                    Laterality = seriesRecord.Get<string>(DicomTag.Laterality) ?? "",
                    SeriesNumber = seriesRecord.Get<string>(DicomTag.Laterality) ?? ""
                };

                sr.SeriesName = seriesRecord.Get<string>(DicomTag.SeriesInstanceUID);
                sr.Images = ImageRecord(seriesRecord).OrderBy(i => i.Index).ToList();
                yield return sr;

            }
        }

        private IEnumerable<ImageRecord> ImageRecord(DicomDirectoryRecord seriesRecord)
        {
            int c = 0;
            int ip = 1;
            foreach (var imageRecord in seriesRecord.LowerLevelDirectoryRecordCollection)
            {
                var ima = new ImageRecord();
                ima.Index = imageRecord.Get<int>(DicomTag.ImageIndex);
                ima.SliceSpace = imageRecord.Get<double>(DicomTag.SpacingBetweenSlices);
                ima.PixelSpacing = imageRecord.Get<double>(DicomTag.PixelSpacing);
                ima.Path = imageRecord.Get<string>(DicomTag.ReferencedFileID);
                ima.ReferencedSOPInstanceUIDInFile = imageRecord.Get<string>(DicomTag.ReferencedSOPInstanceUIDInFile);
                var sp = ima.ReferencedSOPInstanceUIDInFile.Split('.');
                if (file_z.Count(i => i.Key == ima.ReferencedSOPInstanceUIDInFile) < 1)
                    file_z.Add(ima.ReferencedSOPInstanceUIDInFile, ip);
                ip++;
                try
                {
                    //ima.Index = int.Parse(sp[sp.Length - 1]);
                }
                catch (OverflowException e)
                {
                    Console.WriteLine(e.Message);
                    ima.Index = c;
                    c++;
                }
                yield return ima;

            }
        }

        private IEnumerable<string> ExtractFileRefrence()
        {
            foreach (var i in Paitent)
                foreach (var ip in i.Study)
                    foreach (var id in ip.Series)
                        yield return Dir + id.Images.First().Path;
        }
        private IEnumerable<string> ImagePath()
        {
            string[] pr = { };
            foreach (var i in ExtractFileRefrence())
            {
                try
                {
                    pr = System.IO.Directory.GetFiles(i, "*.*", System.IO.SearchOption.AllDirectories);
                }
                catch
                {

                }
                foreach (var p in pr)
                    yield return p;
            }



        }

        private void Setnumbers()
        {

        }
        private void PrepareDictionary()
        {
            // var Paths = ImagePath().Distinct();

            foreach (var item in ImagePath().Distinct())
            {
                try
                {
                    var f = new FileInfo(item);
                    if (!f.Exists)
                        continue;
                    var t = new DicomImage(item).PixelData != null;
                    if (t)
                    {
                        var image = new DicomImage(item);
                        var iop = image.Dataset.Get<string>(DicomTag.SOPInstanceUID);
                        if (iop != null)
                            FileInfoDic.Add(iop, item);
                    }
                }
                catch
                {

                }
            }
        }
        private string DirectoryOfDicomDir()
        {
            var c = Path.Split('\\');
            string np = "";
            for (var i = 0; i < c.Length - 1; i++)
            {
                np += c[i] + "\\";

            }
            return np;
        }
        public void ExtractImages(string path)
        {

            foreach (var i in Paitent)
            {
                var s = path + "\\" + i.PaitentId + "\\";
                DirectoryInfo d = new DirectoryInfo(s);
                if (!d.Exists)
                    d.Create();

                foreach (var p in i.Study)
                {
                    var np = s + p.StudyName + "\\";
                    d = new DirectoryInfo(np);
                    if (!d.Exists)
                        d.Create();
                    foreach (var ip in p.Series)
                    {
                        var npp = s + ip.SeriesName + "\\";
                        d = new DirectoryInfo(npp);
                        if (!d.Exists)
                            d.Create();
                        // int cou = 1;
                        foreach (var io in ip.Images)
                        {
                            var nppi = npp;

                            if (this.FileInfoDic.Count(iz => iz.Key == io.ReferencedSOPInstanceUIDInFile) > 0)
                            {
                                try
                                {
                                    var val = this.FileInfoDic.FirstOrDefault(iz => iz.Key == io.ReferencedSOPInstanceUIDInFile).Value;
                                    var image = new DicomImage(val);
                                    // File.Copy(val, nppi+"a" + cou + ".dcm",false);
                                    // cou++;
                                    // file = new BitmapWithMetaData(image.RenderImage(), ee);
                                    image.RenderImage().Save(nppi + io.ReferencedSOPInstanceUIDInFile + ".jpg", ImageFormat.Jpeg);
                                }
                                catch (Exception ex)
                                {
                                    Console.Write(ex.Message);
                                }

                            }
                        }
                    }
                }
            }
        }

        public async Task<bool> ExtractTempImagesAsync(string path)
        {
            await Task.Factory.StartNew(() =>
            {
                foreach (var i in Paitent)
                {
                    var s = path + i.PaitentId + "\\";
                    DirectoryInfo d = new DirectoryInfo(s);
                    if (!d.Exists)
                        d.Create();
                    foreach (var p in i.Study)
                    {
                        var np = s + p.StudyName + "\\";
                        d = new DirectoryInfo(np);
                        if (!d.Exists)
                            d.Create();
                        foreach (var ip in p.Series)
                        {
                            var npp = s + ip.SeriesName + "\\";
                            d = new DirectoryInfo(npp);
                            if (!d.Exists)
                                d.Create();
                            //  int cou=1;
                            foreach (var io in ip.Images)
                            {
                                var nppi = npp;

                                if (this.FileInfoDic.Count(iz => iz.Key == io.ReferencedSOPInstanceUIDInFile) > 0)
                                {
                                    try
                                    {
                                        var val = this.FileInfoDic.FirstOrDefault(iz => iz.Key == io.ReferencedSOPInstanceUIDInFile).Value;
                                        var image = new DicomImage(val);
                                        // File.Copy(val, nppi+"a" + cou + ".dcm",false);
                                        // cou++;
                                        // file = new BitmapWithMetaData(image.RenderImage(), ee);
                                        image.RenderImage().Save(nppi + io.ReferencedSOPInstanceUIDInFile + ".jpg", ImageFormat.Jpeg);
                                    }
                                    catch (Exception ex)
                                    {
                                        Console.Write(ex.Message);
                                    }

                                }
                            }
                        }
                    }
                }
            });
            return true;
        }

        public string GetTemporaryDirectory()
        {
            string tempFolder = System.IO.Path.GetTempFileName();
            File.Delete(tempFolder);
            Directory.CreateDirectory(tempFolder);
            AddDirectorySecurity(tempFolder, System.Security.Principal.WindowsIdentity.GetCurrent().Name, FileSystemRights.FullControl, AccessControlType.Allow);
            //var Dir = new DirectoryInfo(tempFolder.Split('.')[0]);
            //if(Dir.Exists)
            //Dir.SetAccessControl(new DirectorySecurity(Dir.FullName, AccessControlSections.All));
            return tempFolder;////.Split('.')[0];
        }
        public static void AddDirectorySecurity(string FileName, string Account, FileSystemRights Rights, AccessControlType ControlType)
        {
            // Create a new DirectoryInfo object.
            DirectoryInfo dInfo = new DirectoryInfo(FileName);

            // Get a DirectorySecurity object that represents the  
            // current security settings.
            DirectorySecurity dSecurity = dInfo.GetAccessControl();

            // Add the FileSystemAccessRule to the security settings. 
            dSecurity.AddAccessRule(new FileSystemAccessRule(Account,
                                                            Rights,
                                                            ControlType));

            // Set the new access settings.
            dInfo.SetAccessControl(dSecurity);

        }
    }
    public class StudyRecord
    {
        public StudyRecord()
        {
            Series = new List<SeriesRecord>();
        }
        public string StudyName { get; set; }
        public string StudyDate { get; set; }
        public string StudyTime { get; set; }
        public string ReferringPhysicianName { get; set; }
        public string StudyID { get; set; }
        public string AccessionNumber { get; set; }
        public List<SeriesRecord> Series { get; set; }


    }
    public class PaitentRecord
    {
        public PaitentRecord()
        {
            Study = new List<StudyRecord>();
        }
        public string PaitentName { get; set; }
        public string PaitentId { get; set; }
        public string DateOfBirth { get; set; }
        public string Diagnosis { get; set; }
        public string Procedure { get; set; }
        public string Remarks { get; set; }
        public string Sex { get; set; }
        public List<StudyRecord> Study { get; set; }
    }
    public class SeriesRecord
    {
        public SeriesRecord()
        {
            this.Images = new List<ImageRecord>();
        }
        public string SeriesNumber { get; set; }
        public string Laterality { get; set; }
        public string SeriesName { get; set; }
        public List<ImageRecord> Images { get; set; }
    }

    public class ImageRecord
    {
        public string Path { get; set; }
        public string ReferencedSOPInstanceUIDInFile { get; set; }
        public int Index { get; set; }
        public double PixelSpacing { get; set; }
        public double SliceSpace { get; set; }

    }
}
