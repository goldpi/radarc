﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace RADARC.Dicom
{
    public class PlusPoints
    {

        public static System.Windows.Media.PointCollection Plus()
        {
            PointCollection Points = new PointCollection();
         
            Points.Add(new Point(50,50));//-------------
            Points.Add(new Point(100,50));
            //Line from center to last
            Points.Add(new Point(98,50));
            Points.Add(new Point(98,52));
            Points.Add(new Point(98,50));
            Points.Add(new Point(98,48));
            Points.Add(new Point(98, 50));
            Points.Add(new Point(0, 50));
            Points.Add(new Point(2, 50));
            Points.Add(new Point(2, 52));
            Points.Add(new Point(2, 50));
            Points.Add(new Point(2, 48));
            Points.Add(new Point(2, 50));

            Points.Add(new Point(50, 50));
            Points.Add(new Point(50, 0));
            Points.Add(new Point(50, 2));
            Points.Add(new Point(48, 2));
            Points.Add(new Point(50, 2));
            Points.Add(new Point(52, 2));
            Points.Add(new Point(50, 2));
            Points.Add(new Point(50, 50));

            Points.Add(new Point(50, 100));
           
            Points.Add(new Point(50, 98));
            Points.Add(new Point(48, 98));
            Points.Add(new Point(50, 98));
            Points.Add(new Point(52, 98));
            Points.Add(new Point(50, 98));
            Points.Add(new Point(50, 50));







            //Points.Add(new Point(0, 30));
            //Points.Add(new Point(30, 30));
            //Points.Add(new Point(30, 0));
            //Points.Add(new Point(40,0));
            //Points.Add(new Point(40,30));
            //Points.Add(new Point(70,30));
            //Points.Add(new Point(70, 40));
            //Points.Add(new Point(40,40));
            //Points.Add(new Point(40, 70));
            //Points.Add(new Point(30, 70));
            //Points.Add(new Point(30, 40));
            //Points.Add(new Point(0,40));
            //Points.Add(new Point(0, 30));
            return Points;
        }
    }
}
